# Avaliação Front-End

**Objetivo**

Objetivo deste teste é avaliar seus conhecimentos em organização, estilo, boas práticas e habilidades em front end.

**Requisitos**

- HTML.
- CSS (pré-processador a sua escolha).
- JS (React, Angular ou Vue).
- Desenvolver a página conforme o layout apresentado.
- Suporte para IE9+, Chrome, Safari, Firefox+.
- Responsivo.

**O Desafio**

Seu objetivo é criar um simples app que deve conter duas páginas e um menu de navegação, uma página que exibe um formulário com os campos abaixo, e outra que liste os dados cadastrados.

- Nome completo;
- CPF;
- Telefone;
- Email;
- Endereço;
- Mensagem;

**Instruções:**

- Deve ser possível criar, listar e excluir os dados cadastrados pelo formulário;
- Fazer a persistência dos dados em um localStorage;
- Criar o layout baseado no logo da Fenox - www.fenoxtec.com.br;
- A página deve ser responsiva;
- Para a interação do formulário pode utilizar qualquer framework;

**O que esperamos:**

- Dê suporte a IE10+, Chrome, Safari e Firefox;
- Padrão de Projeto e boas práticas;
- Crie um passo a passo de como rodar sua aplicação (Sugestão);
- Crie uma breve descrição da solução utilizada;

**Finalizando**

Suba a sua proposta para o projeto que você criou no GitHub. Exemplo: https://gitlab.com/seuNome; Aguarde o RH entrar em contato.
